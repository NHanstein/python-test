from ..math import mul
from random import randint

def test_spec():
    assert mul(2, 2) == 4
    assert mul(4, 5) == 20

def test_neutral():
    a = randint(1, 100)
    assert mul(a, 1) == a
    assert mul(1, a) == a

def test_inverse():
    assert mul(2, 0.5) == 1
    assert mul(0.5, 2) == 1
    assert mul(5, 0.2) == 1
    assert mul(0.2, 5) == 1

def test_commut():
    assert mul(4, 5) == mul(5, 4)
    assert mul(3, 6) == mul(6, 3)
    # try with some random ones as well
    a = randint(1, 100)
    b = randint(1, 100)
    assert mul(a, b) == mul(b, a)

def test_assoc():
    assert mul(4, mul(5, 3)) == mul(mul(4, 5), 3)
    assert mul(7, mul(1, 1)) == mul(mul(7, 1), 1)
    # try with some random ones as well
    a = randint(1, 100)
    b = randint(1, 100)
    c = randint(1, 100)
    assert mul(a, mul(b, c)) == mul(mul(a, b), c)
