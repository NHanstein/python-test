from ..math import add
from random import randint

def test_spec():
    assert add(2, 2) == 4
    assert add(4, 5) == 9

def test_neutral():
    a = randint(1, 100)
    assert add(a, 0) == a
    assert add(0, a) == a

def test_inverse():
    assert add(-2, 2) == 0
    assert add(2, -2) == 0
    assert add(-5, 5) == 0
    assert add(5, -5) == 0
    # try with a random one as well
    a = randint(1, 100)
    assert add(-a, a) == 0
    assert add(a, -a) == 0

def test_commut():
    assert add(4, 5) == add(5, 4)
    assert add(3, 6) == add(6, 3)
    # try with some random ones as well
    a = randint(1, 100)
    b = randint(1, 100)
    assert add(a, b) == add(b, a)

def test_assoc():
    assert add(4, add(5, 3)) == add(add(4, 5), 3)
    assert add(7, add(1, 1)) == add(add(7, 1), 1)
    # try with some random ones as well
    a = randint(1, 100)
    b = randint(1, 100)
    c = randint(1, 100)
    assert add(a, add(b, c)) == add(add(a, b), c)
