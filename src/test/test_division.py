from ..math import div
from random import randint

def test_spec():
    assert div(5, 2) == 2
    assert div(4, 2) == 2
    assert div(1, -4) == -1

def test_neutral():
    assert div(5, 1) == 5
    assert div(13, 1) == 13
    assert div(3, 1) == 3
    a = randint(1, 100)
    assert div(a, 1) == a

def test_inverse():
    assert div(2, 2) == 1
    assert div(-2, 2) == -1
    assert div(5, 5) == 1
    assert div(5, -5) == -1

def test_nocommut():
    a = randint(1, 100)
    b = randint(1, 100)
    # Find one that isn't equal to a
    while b == a:
        b = randint(1, 100)
    assert div(a, b) != div(b, a)
