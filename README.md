# A simple Python-based GitLab CI test
[![pipeline status](https://gitlab.com/NHanstein/python-test/badges/master/pipeline.svg)](https://gitlab.com/NHanstein/python-test/pipelines)
[![coverage report](https://gitlab.com/NHanstein/python-test/badges/master/coverage.svg)](https://nhanstein.gitlab.io/python-test/)

This test repo is here for me to explore some of the possibilities provided by GitLab CI.
