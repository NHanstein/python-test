from ..math import sub
from random import randint

def test_spec():
    assert sub(5, 3) == 2
    assert sub(4, -2) == 6
    assert sub(1, 4) == -3

def test_neutral():
    a = randint(1, 100)
    assert sub(a, 0) == a
    assert sub(0, a) == -a

def test_inverse():
    assert sub(2, -2) == 4
    assert sub(-2, 2) == -4
    assert sub(5, -5) == 10
    assert sub(-5, 5) == -10

def test_nocommut():
    a = randint(1, 100)
    b = randint(1, 100)
    # Find one that isn't equal to a
    while b == a:
        b = randint(1, 100)
    assert sub(a, b) != sub(b, a)

def test_commutabs():
    a = randint(1, 100)
    b = randint(1, 100)
    assert abs(sub(a, b)) == abs(sub(b, a))
