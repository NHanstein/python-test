from ..math import fdiv
from random import randint

def test_spec():
    assert fdiv(3, 4) == 0.75
    assert fdiv(1, -4) == -0.25

def test_neutral():
    assert fdiv(10, 1) == 10
    assert fdiv(5, 1) == 5
    assert fdiv(16, 1) == 16
    a = randint(1, 100)
    assert fdiv(a, 1) == a

def test_inverse():
    assert fdiv(2, 2) == 1
    assert fdiv(2, -2) == -1
    assert fdiv(5, 5) == 1
    assert fdiv(-5, 5) == -1

def test_nocommut():
    a = randint(1, 100)
    b = randint(1, 100)
    # find an int that is not a
    while a == b:
        b = randint(1, 100)
    assert fdiv(a, b) != fdiv(b, a)
